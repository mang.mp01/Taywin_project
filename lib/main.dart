import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:taywin_project/home.dart';
import 'package:taywin_project/measure_foot_size.dart';

List<CameraDescription> cameras = [];

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) async {
    try {
      cameras = await availableCameras();
    } on CameraException catch (e) {
      // ignore: avoid_print
      print('Error in fetching the cameras: $e');
    }
    final firstCamera = cameras.first;

    runApp(
      MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          //  brightness: Brightness.dark,
          primaryColor: const Color.fromRGBO(30, 29, 89, 1),
          elevatedButtonTheme: ElevatedButtonThemeData(
            style: ElevatedButton.styleFrom(
              onPrimary: Colors.white,
              primary: const Color.fromRGBO(30, 29, 89, 1),
            ),
          ),
        ),
        home: 
      //  const MeasureFootSize()
        MyHome(
          camera: firstCamera,
        ),
      ),
    );
  });
}
